package com.example.student.recyrcleview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class content_add extends AppCompatActivity {

    public static final String EXTRA_TEXT = "extra_text";

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_add);

        editText = findViewById(R.id.input);

        findViewById(R.id.action)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_TEXT, editText.getText().toString());
                        setResult(RESULT_OK, intent);
                        onBackPressed(); // завершает активити если есть фрагмент
//                        finish(); //закрывает активити в любом случае

                    }
                });
    }
}
